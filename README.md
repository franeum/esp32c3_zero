# esp32c3_zero



## Installing micropython

1.  install `esptool` with this command:
```bash
pip install esptool
```
2.  download micropython firmware from [here](https://micropython.org/resources/firmware/ESP32_GENERIC_C3-20240602-v1.23.0.bin)
3.  connect your `esp32c3 zero` via USB and press and hold the BOOT button then press RESET while the BOOT is still pressed to enter `download mode`
4.  identify serial port (probably `/dev/ttyACM0` on linux) and from a terminal execute this command to erase the flash:
```bash
esptool.py --chip esp32c3 --port /dev/ttyACM0 erase_flash
```
5.  execute this command to upload micropython firmware:
```bash
esptool.py --chip esp32c3 --port /dev/ttyACM0 write_flash -z 0x0 ESP32_GENERIC_C3-20240602-v1.23.0.bin
```
6.  reset the board with RESET button

## Using thonny to interact with the board

1. install thonny with `pip install thonny` or downloading binaries [here](https://thonny.org/)
2. open thonny and go to Tools/Options/Interpreter, choose `MicroPython (generic)` from the first menu, and choose the right port from the second. Press ok.
3. In tha main window press STOP red button and start to work!

## Add files to the board
1. to add files (packages, modules or other) you can you `ampy`, an adafruit (python) program. Install it following [this guid](https://github.com/scientifichackers/ampy)